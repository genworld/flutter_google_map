class ClientTest {
  final String companyName;
  final String adress;
  final String postalCode;
  final String city;
  final double lat;
  final double lng;
  final int orderPassage;

  ClientTest(
      this.companyName,
      this.adress,
      this.postalCode,
      this.city,
      this.lat,
      this.lng,
      this.orderPassage);
}