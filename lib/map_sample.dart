

import 'package:flutter/material.dart';
import 'package:flutter_google_map/client_test_model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {

  List<ClientTest> initialClient = []
    ..add(ClientTest(

      'Micropole Wide',
      '91-95 rue Carnot',
      '92300',
      'Levallois',
      48.897684,
      2.284233,
      1
    ))
    ..add(ClientTest(
        'Air Liquide',
        '28 rue d\'Arcueil',
        '94250',
        'Gentilly',
        48.813962,
        2.344879,
        2
    ))
    ..add(ClientTest(

    'SFR',
    '5 place Jean Jaures',
    '93380',
    'Pierrefitte',
     48.936405,
     2.357015,
    3
    ))
    ..add(ClientTest(
    'Novotel',
    '1 avenue de la République',
    '93170',
    'Bagnolet',
    48.863471,
    2.415296,
    4
    ));



  static final CameraPosition _kCenterParis = CameraPosition(
    target: LatLng(48.859950, 2.339851),
    zoom: 10.4746,
  );

  String clientSelected = '';
  bool clientIsSelected = false;
  GoogleMapController controller;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  MarkerId selectedMarker;
  //List<BitmapDescriptor> _markersIcon = [];
  BitmapDescriptor _markerIcon;
  int _markerIdCounter = 1;

  void _onMapCreated(GoogleMapController controller) {

    this.controller = controller;

    for( var i = 0; i < initialClient.length; i++){
      _add(initialClient[i], i);
    }

  }

  @override
  Widget build(BuildContext context) {
    //_createMarkerImageFromAsset(context);
    return Scaffold(

      body: Stack(
        children:[
          GoogleMap(
            onMapCreated: _onMapCreated,
            mapType: MapType.hybrid,
            myLocationEnabled: true,
            initialCameraPosition: _kCenterParis,
            // TODO(iskakaushik): Remove this when collection literals makes it to stable.
            // https://github.com/flutter/flutter/issues/28312
            // ignore: prefer_collection_literals
            markers: Set<Marker>.of(markers.values),
          ),
          clientIsSelected ? Positioned(
            left: 25,
            bottom: 55,
            width: MediaQuery.of(context).size.width - 50,
            height: 150,
            child: Card(
              color: Colors.white,
              child: Center(child: Text(clientSelected, style: TextStyle( fontSize: 18.0,color: Colors.black),)),
            ),
          )
          :
          Container(),
     ] ),
    );

  }


  @override
  void dispose() {
    super.dispose();
  }

  void _onMarkerTapped(MarkerId markerId) {

    if(clientSelected == markerId.value || clientSelected == ''){
      clientIsSelected = ! clientIsSelected;
    }
    setState(() {
      clientSelected = clientIsSelected ? markerId.value : '';
    });

  }

  void _add( ClientTest client, int index) async {
    final int markerCount = markers.length;

    if (markerCount == 12) {
      return;
    }

    final String markerIdVal = '${client.companyName}\n';
    final String markerSnippet = '${client.adress}\n${client.postalCode} ${client.city}\nOrdre de passage : ${client.orderPassage}';
    _markerIdCounter++;
    final MarkerId markerId = MarkerId(markerIdVal + markerSnippet);
    //_markerIcon = _markersIcon[0];
    final Marker marker = Marker(
      markerId: markerId,
      icon: _markerIcon,
      position: LatLng(
        client.lat, client.lng
      ),
//      infoWindow: InfoWindow(title: markerIdVal, snippet: markerSnippet),
      onTap: () {
        _onMarkerTapped(markerId);
      },
    );

    setState(() {
      markers[markerId] = marker;
    });
  }

  /*Future<void> _createMarkerImageFromAsset(BuildContext context) async {
    if (_markersIcon.length == 0) {
      final ImageConfiguration imageConfiguration =
      createLocalImageConfiguration(context);
      for( var i = 0; i < initialClient.length; i++){
        BitmapDescriptor.fromAssetImage(
            imageConfiguration, 'assets/marker_$i.png')
            .then(_updateBitmap);
      }
    }
  }

  void _updateBitmap(BitmapDescriptor bitmap) {
    _markerIcon = bitmap;
    //_markersIcon.add(bitmap);

  }

  void _displayMarkersClients(){
    for( var i = 0; i < initialClient.length; i++){
      _add(initialClient[i], i);
    }

  }*/

//  Future<void> _goToOurClient() async {
//    final GoogleMapController controller = await _controller.future;
//    controller.animateCamera(CameraUpdate.newCameraPosition(_kCenterParis));
//  }
}